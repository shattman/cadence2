/**********************************************
* File: Supp.h
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*
* Contains the function prototypes for the supp.cpp driver
**********************************************/
#ifndef SUPP_H
#define SUPP_H
#include <iostream>
#include <fstream>
#include "DLList.h"
#include "DLLNode.h"

/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream &stream, char* fileName
* Post-conditions: none
*
* Tries to open a file, and checks if it actually can be opened.
********************************************/
void getFileStream(std::ifstream &stream, char* fileName);

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3
* Post-conditions: none
*
* Adds data into the nodes that are passed into the function
********************************************/
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3);

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, DLList<int>& theList
* Post-conditions: none
*
* Inserts the data from the input stream into our DLList.
********************************************/
void addNodes(std::ifstream& inputStream, DLList<int>& theList);

/********************************************
* Function Name  : setPtrs
* Pre-conditions : Node<int>* node, Node<int>* prev, Node<int>* next
* Post-conditions: none
*
* Sets the pointers for a node in a linked list
********************************************/
void setPtrs(Node<int>* node, Node<int>* prev, Node<int>* next);

/********************************************
* Function Name  : printPtrs
* Pre-conditions : std::ostream& out, Node<int>* node
* Post-conditions: none
*
* Takes the pointers within the given node and prints them to the stdout
********************************************/
void printPtrs(std::ostream& out, Node<int>* node);

#endif
