/**********************************************
* File: Supp.cpp
* Author: Chris Gotuaco, Wonseok Lee, Max Nguyen, Matthew Shan
* Email: cgotuaco@nd.edu, wlee11@nd.edu, mnguye18@nd.edu, mshan@nd.edu
*
* defines the functions used in the rest of the files
**********************************************/
#include "Supp.h"
/********************************************
* Function Name  : getFileStream
* Pre-conditions : std::ifstream &inFile, char* fileName
* Post-conditions: none
*
* Tries to open a file, and checks if it actually can be opened.
********************************************/
void getFileStream(std::ifstream &inFile, char* fileName){
    // Create ifstream and open the stream with the file
    inFile.open(fileName);

    // Check if the file is open.
    if (!inFile.is_open()){
        std::cerr << "File Not Found! Exiting program..." << std::endl;
        exit(-1);
    }
}

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3
* Post-conditions: none
*
* Adds data into the nodes that are passed into the function
********************************************/
void addNodes(std::ifstream& inputStream, Node<int>* node1, Node<int>* node2, Node<int>* node3){
    inputStream >> node1->data;
    inputStream >> node2->data;
    inputStream >> node3->data;
}

/********************************************
* Function Name  : addNodes
* Pre-conditions : std::ifstream& inputStream, DLList<int>& theList
* Post-conditions: none
*
* Inserts the data from the input stream into our DLList.
********************************************/
void addNodes(std::ifstream& inputStream, DLList<int>& theList){
    int data;
    while(inputStream >> data){
        theList.insert(data);
    }
}

/********************************************
* Function Name  : setPtrs
* Pre-conditions : Node<int>* node, Node<int>* prev, Node<int>* next
* Post-conditions: none
*
* Sets the pointers for a node in a linked list
********************************************/
void setPtrs(Node<int>* node, Node<int>* prev, Node<int>* next){
    node->prev = prev;
    node->next = next;
}

/********************************************
* Function Name  : printPtrs
* Pre-conditions : std::ostream& out, Node<int>* node
* Post-conditions: none
*
* Takes the pointers within the given node and prints them to the stdout
********************************************/
void printPtrs(std::ostream& out, Node<int>* node){
    if(node->prev != nullptr){
        out << "Previous: " << node->prev->data << std::endl;
    }
    if(node != nullptr){
        out << "Current: " << node->data << std::endl;
    }
    if(node->next != nullptr){
        out << "Next: " << node->next->data << std::endl;
    }
    out << std::endl;
}
